# React + Spring Boot
> React, axios

## 목차
1. [구현 방법](#1-구현-방법)
2. [실행 방법](#2-실행-방법)
3. [결과](#3-결과)

## 1 구현 방법
### 1-1. 환경 및 구조
- 기술
  - React.js
  - axios http library
- 소프트웨어 프로그램 
  - vscode
### 1-2. 코드 구현
1. axios 라이브러리 다운로드
2. `User.js` api 통신
3. `App.js` 컴포넌트 삽입

## 2 실행 방법 
![](endpoint.jpg)

## 3 결과
![](result.jpg)