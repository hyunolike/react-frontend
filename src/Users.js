import React, {useState, useEffect} from "react";
import axios from "axios";

function Users() {
  const [users, setUsers] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    const fetchUsers = async() => {
      try{
        setUsers(null);
        setError(null);
        setLoading(true);
        const response = await axios.get('http://localhost:9090/api/users');
        setUsers(response.data);
      }catch(e) {
        setError(e);
      }
      setLoading(false);
    };
    fetchUsers();
  }, [])

  if(loading) return <div> 로딩 중 ㅠ,ㅠ </div>
  if(error) return <div> 에러 발생 </div>
  if(!users) return null;

  return <ul>
    {users.map(user => 
        <li key={user.id}>이메일 : {user.email} ({user.lastName} {user.firstName}) .. id : {user.id}</li>
      )}
      </ul>
}

export default Users;